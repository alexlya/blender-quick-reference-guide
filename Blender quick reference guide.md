Blender 2.83 Quick Reference Guide

# Navigation

## Mouse and keyboard

| Shortcut                        | Action                                                           |
| ------------------------------- | ---------------------------------------------------------------- |
| `left click`                    | select item                                                      |
| `a`                             | select all                                                       |
| `alt-a`                         | diselect all                                                     |
| `middle click and drag`         | rotate around center of viewport                                 |
| `shift + middle click and drag` | span viewport                                                    |
| `numpad .`                      | zoom on the selected item (aka. "Frame selected")                |
| `roll mouse wheel`              | zoom in/out                                                      |
| `numberpad 0`                   | bring up camera view perspective                                 |
| `ctrl-space`                    | maximize area (toggle)                                           |
| `t` **or** `shift-space`        | tools section (tiny arrow in upper left hand corner of viewport) |
| `n`                             | object's transformation data and other viewport options          |
| `shift + right click`           | manually set 3D cursor                                           |
| `shift-s > 1 (or w)`            | pie-wheel: reset 3D "cursor to world origin"                     |

## Upper-right hand corner items

| Item               | Description                                                                     |
| ------------------ | ------------------------------------------------------------------------------- |
| `RGB gizmo `       | Click or click and drag to rotate around center of viewport.                    |
| `Hand`             | Spans the viewport. Same as `shift + middle click and drag`.                    |
| `Magnifying glass` | Zoom in-and-out. Click and drag up/down. Same as `roll the mouse wheel`.        |
| `Camera`           | Brings up the view of camera. Click to toggle. Also `Numpad 0`.                 |
| `Grid`             | Switch between perspective (realistic) and orthographic views. Click to toggle. |

## Upper-left hand corner items

| Item                    | Description                                    |
| ----------------------- | ---------------------------------------------- |
| `View > Frame selected` | Zoom onto a selected item. Same as `Numpad .`. |

## Bottom stripe with action hints
The most bottom portion of UI hints at what various mouse clicks will do depending on which section of UI you are hovering over.

## Workspaces
At the top of UI there are `Workspaces` such as `Layout`, `Modeling`, `Scupting`, etc. The provide custom UI for spesific purpose.

## Pie-wheels
Pie-wheel are quick wheel menues which appear around your cursor after a hotkey is pressed. There are 3 options when navigating a pie menu:
- right click to cancel
- press and hold hotkey, drag mouse to desired operation, release hotkey to select
- press hotkey and then left click, or click number, or click highlighted letter

## Properties editor
Contains vertical button to handle properties of your scene.

Top to bottom :
1. options of currently selected object or tool in viewport
2. scene options (scene can added/removed/selected in the top bar, closer to the right)
   1. render tab (back facing camera icon)
   2. output tab (printer icon)
   3. view layer tab (stack of photos icon)
   4. general scene tab (cone and spheres icon)
   5. world tab (globe icon)
3. this section is spesific for the object that is currently selected
   1. object tab (square icon)
   2. modifiers tab (wrench) VERY IMPORTANT TAB for blender
   3. particles tab (constalation icon)
   4. physics tab (orbiting plants icon)
   5. constaints tab (two object connected together icon)
   6. mesh data (green triangle icon)
   7. material tab (beach ball icon)
4. texture tab (chess board icon)
5. lamp properties (if lamp is selected) (green lightbulb icon)
6. camera options (if camera is selected) (green camera icon)